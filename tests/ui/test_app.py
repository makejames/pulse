"""Test the endpoints on the UI."""

from fastapi.testclient import TestClient

from pulse.ui.app import app


client = TestClient(app)

def test_read_main():
    response = client.get("/")
    assert response.status_code == 200

def test_read_styles():
    response = client.get("/static/style.css")
    assert response.status_code == 200
