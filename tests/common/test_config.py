"""Test the methods of the Config classes."""

from pathlib import Path

from pulse.common.config import ConfigManager


def test_config_creation(tmp_path) -> None:
    """R-BICEP: Right."""
    test_config = f"{tmp_path}/.tmp_config"

    assert Path(test_config).exists() is False

    ConfigManager(config_path=test_config)

    assert Path(test_config).exists()

def test_config_fields(tmp_path) -> None:
    """R-BICEP: Right."""
    test_config = f"{tmp_path}/.tmp_config"

    config = ConfigManager(config_path=test_config).config

    assert config.core.name == "Example Status Page"
