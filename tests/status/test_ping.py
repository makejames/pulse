"""Test the methods of the ping module."""

import pytest
from pathlib import Path
from pulse.status.ping import PingManager

@pytest.mark.asyncio
async def test_ping_success():
    """R-BICEP: Right."""
    manager = PingManager(['127.0.0.1'])
    result = await manager.ping('127.0.0.1')
    assert result.status == 'online'
    assert result.min_time is not None
    assert result.avg_time is not None
    assert result.max_time is not None

@pytest.mark.asyncio
async def test_ping_failure():
    """R-BICEP: Boundary."""
    manager = PingManager(['nonexistenthost'])
    result = await manager.ping('nonexistenthost')
    assert result.status == 'offline'
    assert result.min_time is None
    assert result.avg_time is None
    assert result.max_time is None

def test_parse_output_success():
    """R-BICEP: Right."""
    manager = PingManager([])
    _path = Path(__file__).parent.resolve()
    with open(f"{_path}/ping-output.txt", "rb") as ping_output:
        output = ping_output.read().decode()
    result = manager.parse_output('example.com', output)
    assert result.status == 'online'
    assert abs(result.min_time - 100.251) < 0.1
    assert abs(result.avg_time - 100.739) < 0.1
    assert abs(result.max_time - 101.271) < 0.1

def test_parse_output_failure():
    """R-BICEP: Boundary."""
    manager = PingManager([])
    output = "Invalid ping output"
    result = manager.parse_output('example.com', output)
    assert result.status == 'offline'
    assert result.min_time is None
    assert result.avg_time is None
    assert result.max_time is None

@pytest.mark.asyncio
async def test_run():
    """R-BICEP: Right."""
    manager = PingManager(['127.0.0.1'])

    results = await manager.run()
    assert len(results) == 1
    assert results[0].status == 'online'
    assert results[0].host == '127.0.0.1'
    assert results[0].min_time > 0
    assert results[0].avg_time > 0
    assert results[0].max_time > 0
