"""Test the methods of the http module."""

import pytest
from pulse.status.http import HTTPManager, HTTPStats

@pytest.mark.asyncio
async def test_request_success():
    """R-BICEP: Right."""
    manager = HTTPManager(['httpbin.org'])
    result = await manager.request('httpbin.org')
    assert result.status == 'online'
    assert result.response_time is not None
    assert result.response_time >= 0

@pytest.mark.asyncio
async def test_request_failure():
    """R-BICEP: Error."""
    # Test HTTP request failure (non-existent host)
    manager = HTTPManager(['nonexistenthost'])
    result = await manager.request('nonexistenthost')
    assert result.status == 'offline'
    assert result.response_time is None

@pytest.mark.asyncio
async def test_request_timeout():
    """R-BICEP: Boundary."""
    manager = HTTPManager(['httpbin.org'])
    manager.timeout = 0.1  # Set a short timeout for testing
    result = await manager.request('httpbin.org/delay/2')
    assert result.status == 'offline'
    assert result.response_time is None

@pytest.mark.asyncio
async def test_request_status_404():
    """R-BICEP: Boundary."""
    manager = HTTPManager(['httpbin.org'])
    result = await manager.request('httpbin.org/status/404')
    assert result.status == 'offline'
    assert result.status_code == 404
    assert result.response_time is not None

@pytest.mark.asyncio
async def test_request_status_500():
    """R-BICEP: Boundary."""
    manager = HTTPManager(['httpbin.org'])
    result = await manager.request('httpbin.org/status/500')
    assert result.status == 'offline'
    assert result.status_code == 500
    assert result.response_time is not None

def test_http_stats_str():
    """R-BICEP: Right.."""
    # Test __str__ method of HTTPStats
    stats = HTTPStats(host='example.com', status='online', response_time=50.0)
    expected_output = "HTTP statistics for example.com:\nResponse time: 50.0 ms"
    assert str(stats) == expected_output
