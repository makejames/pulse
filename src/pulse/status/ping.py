"""Send and Parse ping requests."""

from typing import List
import asyncio
import re
from pydantic import BaseModel


class PingStats(BaseModel):

    host: str
    status: str = "offline"
    min_time: float|None = None
    max_time: float|None = None
    avg_time: float|None = None

    def __str__(self) -> str:
        return f"Ping statistics for {self.host}:\n" \
               f"Min time: {self.min_time} ms\n" \
               f"Max time: {self.max_time} ms\n" \
               f"Avg time: {self.avg_time} ms"


class PingManager:

    def __init__(self, hosts: List[str]) -> None:
        self.hosts = hosts

    async def ping(self, host: str) -> PingStats:
        # TODO: count should be defined in config.
        count: int = 4
        try:
            process = await asyncio.create_subprocess_shell(
                f"ping -c {count} {host}",
                stdout=asyncio.subprocess.PIPE,
                stderr=asyncio.subprocess.PIPE
            )
            stdout, stderr = await process.communicate()
            return self.parse_output(host, stdout.decode() )
        except Exception as e:
            print(f"Failed to run ping: {e}")
            return PingStats(host=host)

    def parse_output(self, host: str, output: str) -> PingStats:
        match = re.search(
            r'min/avg/max/mdev = ([\d.]+)/([\d.]+)/([\d.]+)/[\d.]+ ms',
            output
        )
        if match:
            return PingStats(
                host=host,
                status="online",
                min_time=float(match.group(1)),
                avg_time=float(match.group(2)),
                max_time=float(match.group(3))
            )
        else:
            print(f"Failed to parse ping output. Assuming {host} is offline")
            return PingStats(host=host)

    async def run(self) -> List[PingStats]:
        return await asyncio.gather(
            *[self.ping(host) for host in self.hosts]
        )
