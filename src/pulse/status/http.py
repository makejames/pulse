"""Request response time."""

import asyncio
import requests
from typing import List
from pydantic import BaseModel

class HTTPStats(BaseModel):

    host: str
    status: str = "offline"
    status_code: int|None = None
    response_time: float|None = None

    def __str__(self) -> str:
        return f"HTTP statistics for {self.host}:\n" \
               f"Response time: {self.response_time} ms"

class HTTPManager:

    def __init__(self, hosts: List[str]) -> None:
        self.hosts: List[str] = hosts # TODO: should be a class object from config
        self.timeout: int = 5   # TODO: pull from config

    async def request(self, host: str) -> HTTPStats:
        try:
            # TODO: establish http / https from config
            url = f"http://{host}"
            response = requests.get(url, timeout=self.timeout)
            if response.status_code == 200:
                return HTTPStats(
                    host=host,
                    status="online",
                    status_code=response.status_code,
                    response_time=response.elapsed.total_seconds() * 1000
                )
            else:
                print(
                    f"HTTP request to {host} failed with"
                    f"status code {response.status_code}"
                )
                return HTTPStats(
                    host=host,
                    status_code=response.status_code,
                    response_time=response.elapsed.total_seconds() * 1000
                )
                return HTTPStats(host=host, status_code=response.status_code)
        except requests.RequestException as e:
            print(f"Failed to make HTTP request to {host}: {e}")
            return HTTPStats(host=host)

    async def run(self) -> List[HTTPStats]:
        return await asyncio.gather(*[self.request(host) for host in self.hosts])
