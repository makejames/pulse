"""Instantiate the configuration of the application."""

from pathlib import Path
import tomllib

from pydantic import BaseModel
import tomli_w

class CoreConfig(BaseModel):

    name: str = "Example Status Page"
    url: str = "https://status.example.com/"


class DatabaseConfig(BaseModel):

    dialect: str = "postgres"
    user: str = "admin"
    password: str = "secret"
    host: str = "localhost"
    port: int = 5432


class UiConfig(BaseModel):

    host: str = "127.0.0.1"
    port: int = 8081


class AppConfig(BaseModel):

    core: CoreConfig = CoreConfig()
    database: DatabaseConfig = DatabaseConfig()
    ui: UiConfig = UiConfig()


class ConfigManager:

    def __init__(self, config_path: str) -> None:
        self.config_path: Path = Path(config_path)
        self.config_dir = self.config_path.parent

        self.config_dir.mkdir(exist_ok=True)

        self.config = self.load_config()
        self.save_config()

    def load_config(self) -> AppConfig:
        if not self.config_path.exists():
            return AppConfig()

        with open(self.config_path, "rb") as config_file:
            config_dict = tomllib.load(config_file)

        return AppConfig(**config_dict)

    def save_config(self) -> None:
        with open(self.config_path, "wb") as config_file:
            tomli_w.dump(self.config.model_dump(), config_file)
