"""Create UI end-points."""

from fastapi import FastAPI, Request
from fastapi.staticfiles import StaticFiles
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates

from pathlib import Path


app = FastAPI(docs_url=None, redoc_url=None)

path = Path(__file__).parent

templates = Jinja2Templates(directory=f"{path}/templates")

app.mount("/static", StaticFiles(directory=f"{path}/static"),
          name="static files")

@app.get("/", response_class=HTMLResponse)
async def index_html(request: Request) -> HTMLResponse:
    return templates.TemplateResponse(
        request=request,
        name="home.html",
        context={}
    )
