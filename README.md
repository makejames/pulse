# Pulse

Pulse is a monitoring solution and status page.
Built in python, this service is designed to be a one-stop-shop for service
and application monitoring.

## Overview

There are 3 core elements to Pulse.

- Status Engine
- Notification Engine
- User Interface

The service stores state in the database
and is configured using a central configuration file.

```mermaid
flowchart LR
    subgraph main[Main server]
        direction LR
        db[(database)]
        subgraph st[status engine]
            cron[cron service]
            ping[ping service]
            http[http/https service]
            in[intput api]
        end
        subgraph not[notifications engine]
            hc[health check]
            email[email service]
        end
        subgraph ui[User Interface]
            temp[jinja templating engine]
        end

        db --> ui

        cron --> ping
        cron --> http

        ping --> db
        http --> db
        in --> db

        db --> hc
    end
    temp --> htmx
    htmx --> ui
    subgraph sub[reporting servier]
        subgraph st2[status engine]
            direction LR
            cron2[cron service]
            ping2[ping service]
            http2[http/https service]
            other[...]
        end
        cron2 --> ping2
        cron2 --> http2
        cron2 --> other
    end
    st2 --> in
```

## Installation

## Development

[![pdm-managed](https://img.shields.io/badge/pdm-managed-blueviolet)](https://pdm-project.org)

## Contributing

