SHELL=/bin/bash
EXECUTE=pdm run

SRC = src/
TESTS = tests/
TEST_GROUP = tests/*/test_*.py

# standard targets

all: install

clean:

install:
	pdm install --prod

test:
	$(EXECUTE) pytest $(TEST_GROUP)

## non-standard targets

build: install
	pdm build

coverage:
	$(EXECUTE) pytest $(TEST_GROUP) --cov $(SRC) --cov-report=term-missing

dev:
	@echo "Installing package and developer dependencies."
	pdm install
	cp pre-commit .git/hooks/pre-commit
	chmod +x .git/hooks/pre-commit

lint:
	$(EXECUTE) ruff check $(SRC) $(TESTS)
	$(EXECUTE) mypy $(SRC)

update:
	pdm update -d

.PHONY: install
